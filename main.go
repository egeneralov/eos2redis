package main

import (
  "encoding/json"
  "fmt"
  "os"
  eos "github.com/eoscanada/eos-go"
  "github.com/go-redis/redis"
)


func getAPIURL() string {
  apiURL := os.Getenv("EOS_API_URL")
  if apiURL != "" {
    return apiURL
  }
  return "https://mainnet.eoscanada.com"
}

func getRedisURL() string {
  apiURL := os.Getenv("REDIS_URL")
  if apiURL != "" {
    return apiURL
  }
  return "localhost:6379"
}


func getBlockAsJson(str string) string {
  api := eos.New(getAPIURL())
  block, _ := api.GetBlockByID(str)
  bytes, _ := json.Marshal(block)
  return string(bytes)
}

func main() {
  api := eos.New(getAPIURL())
  info, _ := api.GetInfo()


  client := redis.NewClient(&redis.Options{
    Addr: getRedisURL(),
    Password: "",
    DB: 0,
  })

  pong, err := client.Ping().Result()
  fmt.Println(pong, err)

  for i:=1; i<=int(info.LastIrreversibleBlockNum); i++ {
    str := fmt.Sprintf("%d", i)


    _, err := client.Get(str).Result()
    if err == redis.Nil {
      fmt.Println("block " + str + " does not exist")

      block := getBlockAsJson(str)
      
      err := client.Set(str, block, 0).Err()
      if err != nil {
        panic(err)
      }

      fmt.Println("block " + str + " cached")
      
    } else if err != nil {
      panic(err)
    } else {
      fmt.Println("block " + str + " exist in cache")
    }

  }

}











