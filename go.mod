module eos2redis

go 1.12

require (
	github.com/eoscanada/eos-go v0.8.16
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/tidwall/gjson v1.3.2 // indirect
	github.com/tidwall/sjson v1.0.4 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0 // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
)
