# FROM golang
FROM golang:alpine

WORKDIR /go/src/gitlab.com/egeneralov/eos2redis

ENV \
  CGO_ENABLED=0 \
  GOOS=linux \
  GOARCH=amd64 \
  GOARM=6 \
  GO111MODULE=on

ADD go.mod go.sum /go/src/gitlab.com/egeneralov/eos2redis/

RUN go mod download

ADD . .

RUN \
  go build \
  -a \
  -v \
  -installsuffix cgo \
  -ldflags="-w -s" \
  -o /go/bin/eos2redis


FROM scratch
# USER nobody
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=0 /go/bin/eos2redis /eos2redis
ENTRYPOINT ["/eos2redis"]
